<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader,
    Bitrix\Main\Context,
    Bitrix\Main\Mail\Event,
    Bitrix\Main\Web\Json;

class CNewForm extends CBitrixComponent
{

    private $formdata;

    public function __construct($component = null)
    {

        parent::__construct($component);
        if(!Loader::IncludeModule("form")){
            ShowError('No Form Module');
            return;
        }
    }

    public function executeComponent()
    {
        if (empty($this->arParams['FORM_ID'])) {
            return;
        }
        $this->loadFormData();
        if ($this->request->getPost('FORM_ID') === $this->arParams['FORM_ID'] && check_bitrix_sessid()) {
            $this->checkForm();
        }
        $this->arResult = $this->formdata;
        $this->includeComponentTemplate();
    }

    private function loadFormData()
    {
        CForm::GetDataByID(
            $this->arParams['FORM_ID'],
            $arForm,
            $arQuestions,
            $arAnswers,
            $arDropDown,
            $arMulti);
        $this->formdata = [
            'arForm' => $arForm,
            'arQuestions' => $arQuestions
        ];
    }

    private function checkForm() {
        $response = Context::getCurrent()->getResponse();
        $response->addHeader('Content-Type', 'application/json');
        $values = $this->request->getPostList()->toArray();
        global $APPLICATION;
        $APPLICATION->RestartBuffer();
        $errors = CForm::Check($this->arParams["WEB_FORM_ID"], $values, false, "Y", 'N');
        if (!empty($errors)) {
            try {
                $errors = Json::encode($errors);
            } catch (\Bitrix\Main\ArgumentException $e) {
                ShowError($e->getMessage());
                die;
            }
            $response->flush($errors);
            die;
        } else {
            Event::send([
                "EVENT_NAME" => $this->arParams['MAILEVENT_NAME'],
                "LID" => SITE_ID,
                "C_FIELDS" => array(
                    "EMAIL" => COption::GetOptionString('main', 'email_from', 'default@admin.email'),
                    'DATA' => $values
                ),
            ]);
        }
    }
}